/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.woramet.oxprogram;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author User
 */
public class OXProgramTest {

    public OXProgramTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckVerticlePlayerOCol1Win() {
        char table[][] = {{'O', '-', '-'}, 
                                   {'O', '-', '-'}, 
                                   {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;
        assertEquals(true,OXProgram.checkVertical(table, currentPlayer, col));
    }
    
    @Test
     public void testCheckVerticlePlayerOCol2Win() {
        char table[][] = {{'-', 'O', '-'}, 
                                   {'-', 'O', '-'}, 
                                   {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int col = 2;
        assertEquals(true,OXProgram.checkVertical(table, currentPlayer, col));
    }
     
     @Test
     public void testCheckVerticlePlayerOCol3Win() {
        char table[][] = {{'-', '-', 'O'}, 
                                   {'-', '-', 'O'}, 
                                   {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 3;
        assertEquals(true,OXProgram.checkVertical(table, currentPlayer, col));
    }
     
     @Test
     public void testCheckHorizontalPlayerORow1Win() {
        char table[][] = {{'O', 'O', 'O'}, 
                                   {'-', '-', '-'}, 
                                   {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 1;
        assertEquals(true,OXProgram.checkHorizontal(table, currentPlayer, row));
    }
     
     @Test
     public void testCheckHorizontalPlayerORow2Win() {
        char table[][] = {{'-', '-', '-'}, 
                                   {'O', 'O', 'O'}, 
                                   {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 2;
        assertEquals(true,OXProgram.checkHorizontal(table, currentPlayer, row));
    }
     
     @Test
     public void testCheckHorizontalPlayerORow3Win() {
        char table[][] = {{'-', '-', '-'}, 
                                   {'-', '-', '-'}, 
                                   {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        int row = 3;
        assertEquals(true,OXProgram.checkHorizontal(table, currentPlayer, row));
    }
     
     @Test
     public void testCheckXPlayerOX1Win() {
        char table[][] = {{'O', '-', '-'}, 
                                   {'-', 'O', '-'}, 
                                   {'-', '-', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true,OXProgram.checkX(table, currentPlayer));
    }
     
     @Test
     public void testCheckXPlayerOX2Win() {
        char table[][] = {{'-', '-', 'O'}, 
                                   {'-', 'O', '-'}, 
                                   {'O', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true,OXProgram.checkX(table, currentPlayer));
    }
     
     @Test
     public void testCheckXPlayerXX2Win() {
        char table[][] = {{'-', '-', 'X'}, 
                                   {'-', 'X', '-'}, 
                                   {'X', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(true,OXProgram.checkX(table, currentPlayer));
    }
     
     @Test
     public void testCheckDraw() {
        int count = 8;
        assertEquals(true,OXProgram.checkDraw(count));
    }
}
