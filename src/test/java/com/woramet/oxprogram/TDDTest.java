/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.woramet.oxprogram;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author User
 */
public class TDDTest {

    public TDDTest() {
    }

    // add(int a,int b) -> int
    // add(1,2) -> 3
    @Test
    public void TestAdd_1_2is3() {
        assertEquals(3, Example.add(1, 2));
    }

    // add(3,4) -> 7
    @Test
    public void TestAdd_3_4is7() {
        assertEquals(7, Example.add(3, 4));
    }

    // add(20,22) -> 42
    @Test
    public void TestAdd_20_22is42() {
        assertEquals(42, Example.add(20, 22));
    }

    // p: paper ,s:scissors ,h:hammer
    // chop(char player1 ,char player2) -> "p1","p2","draw"
    @Test
    public void TestChop_p1_p_p2_p_is_draw() {
        assertEquals("draw", Example.chop('p', 'p'));
    }

    @Test
    public void TestChop_p1_h_p2_h_is_draw() {
        assertEquals("draw", Example.chop('h', 'h'));
    }

    @Test
    public void TestChop_p1_s_p2_s_is_draw() {
        assertEquals("draw", Example.chop('s', 's'));
    }
    
    @Test
    public void TestChop_p1_s_p2_p_is_p1() {
        assertEquals("p1", Example.chop('s', 'p'));
    }
    
    @Test
    public void TestChop_p1_h_p2_s_is_p1() {
        assertEquals("p1", Example.chop('h', 's'));
    }
    
    @Test
    public void TestChop_p1_p_p2_h_is_p1() {
        assertEquals("p1", Example.chop('p', 'h'));
    }
    
     @Test
    public void TestChop_p1_p_p2_s_is_p2() {
        assertEquals("p2", Example.chop('p', 's'));
    }
    
    @Test
    public void TestChop_p1_s_p2_h_is_p2() {
        assertEquals("p2", Example.chop('s', 'h'));
    }
    
     @Test
    public void TestChop_p1_h_p2_p_is_p2() {
        assertEquals("p2", Example.chop('h', 'p'));
    }
}
